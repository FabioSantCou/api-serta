const server = require('./src/Config/server')
require('./src/Config/database')
require('./src/Config/routes')(server)
require('./src/Mqtt/mqttService')
require('./src/Persist/persistMessageTp')