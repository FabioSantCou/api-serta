const request = require("supertest")

// const URL_TEST = process.env.URL_TEST || 'http://192.168.0.106:3000/api'
const URL_TEST = process.env.URL_TEST || 'http://localhost:3003/api'


beforeAll(()=>{
    console.log("Start script test!")
})

test( `Get all messages`,()=>{
    return request (URL_TEST)
    .get("/message")
    .then(response => {
        expect(response.status).toBe(200)
        expect(response.body).toBeInstanceOf(Array)
    }).catch(fail)
})

test( `Get all TP messages TP`,()=>{
    return request (URL_TEST)
    .get("/message/tp")
    .then(response => {
        expect(response.status).toBe(200)
        expect(response.body).toBeInstanceOf(Array)
    }).catch(fail)
})

test( `Get messages with id that don't exists`,()=>{
    return request (URL_TEST)
    .get("/message/sdasdasd")
    .then(response => {
        expect(response.status).toBe(404)
    }).catch(fail)
})

test( `Get messages TP with id that don't exists`,()=>{
    return request (URL_TEST)
    .get("/message/tp/sdasdasd")
    .then(response => {
        expect(response.status).toBe(404)
    }).catch(fail)
})


afterAll(()=>{
    console.log("Finish script test!")
})
