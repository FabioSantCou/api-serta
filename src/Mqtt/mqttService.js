var mqtt = require('mqtt')
const MessageTp = require('../Model/MessageTp/messageTp')
const Message = require('../Model/Message/message')

const BROKER = process.env.BROKER
const USERNAME_BROKER = process.env.USERNAME_BROKER
const PASSWORD_BROKER = process.env.PASSWORD_BROKER
const TOPIC_SUBSCRIBE = process.env.TOPIC_SUBSCRIBE
const TOPIC_SUBSCRIBE_TP = process.env.TOPIC_SUBSCRIBE_TP
const TOPIC_WILL = process.env.TOPIC_WILL
const TOPIC_CALLBACK_ID = process.env.TOPIC_CALLBACK_ID

let securityOptions = {
    username: USERNAME_BROKER,
    password: PASSWORD_BROKER,
      will:{
        topic:TOPIC_WILL,
        payload:"Disconectado",
        qos:"1",
    }  
  }

let idReq=""

var client  = mqtt.connect(BROKER,securityOptions)

client.on('connect', function (connack) {
  client.subscribe(TOPIC_SUBSCRIBE, function (err) {
    if (!err) {
      console.log("MQTT - Conectado - TOPIC_SUBSCRIBE")
      client.publish(TOPIC_WILL,"Conectado")
    }
  })

  client.subscribe(TOPIC_SUBSCRIBE_TP, function (err) {
    if (!err) {
      console.log("MQTT - Conectado - TOPIC_SUBSCRIBE_TP")
      client.publish(TOPIC_WILL,"Conectado")
    }
  })

  client.subscribe(TOPIC_WILL, function (err) {
    if (!err) {
      console.log("MQTT - Conectado - TOPIC_WILL")
    }
  })
})

client.on('message', function (topic, message, packet) {
  console.log("MENSAGEM = "+message.toString())
  if(message.toString()=="Init"){
    client.publish(TOPIC_WILL,"Conectado")
  }else if (topic == TOPIC_SUBSCRIBE){
    let newMessage = createNewMessage(message.toString())
    storeMessageToDB(newMessage)
  }else if (topic == TOPIC_SUBSCRIBE_TP){
    let newMessageTp = createNewMessageTP(message.toString())
    storeMessageTpToDB(newMessageTp)
  }
  
})

client.on('disconnect', function (packet) {
  console.log("disconectado")
})

let createNewMessage = (message) =>{
  const obj = JSON.parse(message.toString())
  console.log(obj.operator)
  let newMessage = new Message(
    {
      operator:obj.operator,
      time:obj.time,
      date:obj.date,
      idTransformer:obj.idTransformer,
      transformer:obj.transformer,
      winding:obj.winding,
      position:obj.position,
      charge:obj.charge,
      current:obj.current,
      result:obj.result,
      approved:obj.approved,
      temperature:obj.temperature,
      humidity:obj.humidity,
      idReq: obj.idReq,
      persisted: obj.persisted
    }
  )
  console.log("MENSAGEM - JSON = "+JSON.stringify(newMessage))
  console.log("ID_REQ = "+ idReq +"\n\n")

  return newMessage
}

let createNewMessageTP = (message) =>{
  const obj = JSON.parse(message.toString())
  console.log(obj.operator)
  let newMessage = new MessageTp(
    {
      operator:obj.operator,
      time:obj.time,
      date:obj.date,
      idTransformer:obj.idTransformer,
      transformer:obj.transformer,
      winding:obj.winding,
      position:obj.position,
      charge:obj.charge,
      relation:obj.relation,
      serialNumber:obj.serialNumber,
      transformerClass:obj.transformerClass,
      result:obj.result,
      approved:obj.approved,
      temperature:obj.temperature,
      humidity:obj.humidity,
      idReq: obj.idReq,
      persisted: obj.persisted
    }
  )
  console.log("MENSAGEM - JSON = "+JSON.stringify(newMessage))
  console.log("ID_REQ = "+ idReq +"\n\n")

  return newMessage
}

let storeMessageToDB = (message) =>{

  message.save(err => {
    if(err) {
        console.log("ERRO ao Salvar Mensagem => "+ err.toString())
    } else {
        console.log("Mensagem gravada com Sucesso!")
        console.log("\n\nID Enviado = "+ message.idReq +"\n\n")
        client.publish(TOPIC_CALLBACK_ID,message.idReq)
        idReq=""
    }
  })

}

let storeMessageTpToDB = (message) =>{

  message.save(err => {
    if(err) {
        console.log("ERRO ao Salvar Mensagem TP => "+ err.toString())
    } else {
        console.log("Mensagem TP gravada com Sucesso!")
        console.log("\n\nID Enviado = "+ message.idReq +"\n\n")
        client.publish(TOPIC_CALLBACK_ID,message.idReq)
        idReq=""
    }
  })

}