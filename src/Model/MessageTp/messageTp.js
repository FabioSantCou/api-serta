const restful = require('node-restful')
const mongoose = restful.mongoose

const messageTpSchema = new mongoose.Schema({
    operator:{type:String},
    time:{type:String},
    date:{type:String},
    idTransformer:{type:String},
    transformer:{type:String},
    winding:{type:String},
    position:{type:String},
    charge:{type:String},
    relation:{type:String},
    serialNumber:{type:String},
    transformerClass:{type:String},
    result:{type:Object},
    approved:{type:String},
    temperature:{type:Number},
    humidity:{type:Number},
    idReq:{type:String},
    persisted:{type:Boolean}
})

module.exports = restful.model('MessageTp',messageTpSchema)