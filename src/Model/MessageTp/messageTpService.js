const MessageTp = require('./messageTp')
const lodash = require('lodash')

MessageTp.methods(['get','post','put','delete'])
MessageTp.updateOptions({new:true, runValidators:true})

MessageTp.after('post', sendErrorsOrNext).after('put',sendErrorsOrNext)

function sendErrorsOrNext(req,resp,next){
    const bundle = resp.locals.bundle

    if(bundle.errors){
        var errors = parseErrors(bundle.errors)
        resp.status(500).json({errors})
    }else{
        next()
    }
}


function parseErrors(nodeRestfulErrors){

    const errors = []
    lodash.forIn(nodeRestfulErrors, error =>{
        errors.push(error.message)
    })
}


module.exports = MessageTp