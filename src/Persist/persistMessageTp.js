const MessageTp = require ("../Model/MessageTp/messageTp")
const HttpStatusCodes = require("../Untils/HttpStatusCodes")

const timeBetweenSendMessages = 10000
const URL_HOST_API_HIGH = process.env.URL_HOST_API_HIGH  //this addres must have the ip from vpn serta.
const API_PORT_HIGH = process.env.API_PORT_HIGH  // port that will run tha api high inside vpn serta
const PATH_API_HIGH = process.env.PATH_API_HIGH


let loopSendedMessagetp = ()=>{
    setTimeout(()=>{
        console.log("Sending Messages_TP!")
        findMessagesTp()
    },(timeBetweenSendMessages))
}

// Function to find messages with field persisted = false
let findMessagesTp = () =>{
    MessageTp.aggregate([
        {$match:{persisted:false}}
    ],(err,messages) => {
        if(err){
            console.log("Error on send messageTP => "+err)
        }else if(messages){
            if(messages == ""){
                console.log("Don't have message to send!")
                loopSendedMessagetp()
            }else{
                sendMessagesTp(messages)
            }
        }
    })
}

let sendMessagesTp = (messages) =>{

    console.log("Messages to send")
    console.log(messages.length)
    console.log(messages)

    let headers = {
        "Content-Type": "application/json",
    };

    jsonObject = {
        Messages:{
            messages
        }
    }

    let options = {
        host: URL_HOST_API_HIGH,
        port: API_PORT_HIGH,
        path: PATH_API_HIGH,
        method: "POST",
        headers: headers
    };
    
    var http = require('http');
    var req = http.request(options, function(res) {
        
        if(res.statusCode == HttpStatusCodes.code.SUCCESS){
            setPersistedMessages(messages)
        }
        
        res.on('data', function(data) {
            console.log("Response:"+data);
        });

        loopSendedMessagetp()
    });
    
    req.on('error', function(e) {
        console.log("ERROR:");
        console.log(e);
        loopSendedMessagetp()
    });
    
    req.write(JSON.stringify(jsonObject));
    req.end();

}

let setPersistedMessages = (messages) =>{
    
    for(let i=0;i<messages.length;i++){
        let ObjectID = require("mongodb").ObjectID
        let messageTp = new MessageTp(messages[i])
        console.log(messageTp._id)
        MessageTp.updateOne(
            { _id: new ObjectID(messages[i]._id) },
            { $set: { "persisted": true } },(err,message)=>{
                if(err) {
                    console.log("Error " + err)
                    return
                }
               else{
                   console.log("MESSAGE = "+JSON.stringify(message))
               } 
               
               if(i == messages.length-1){
                   console.log("Messages updated successfully!")
               }
            }
        )}
}

loopSendedMessagetp()


